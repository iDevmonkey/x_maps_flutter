import 'package:flutter/material.dart';

import 'package:x_maps_flutter/x_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:amap_flutter_map/amap_flutter_map.dart' as aMaps;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Permission> needPermissionList = [
    Permission.location,
    Permission.storage,
    Permission.phone,
  ];

  @override
  void initState() {
    super.initState();
    _checkPermissions();
    XMapsFlutter.init({
      "in_china": true,
      "config": {
        "ios": "key", "android":"key"
      }
    });
  }

  @override
  void reassemble() {
    super.reassemble();
    _checkPermissions();
  }

  void _checkPermissions() async {
    Map<Permission, PermissionStatus> statuses =
    await needPermissionList.request();

    statuses.forEach((key, value) {
      print('$key premissionStatus is $value');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: FlatButton(
            child: const Text('显示地图'),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context)=> MapPage()));
            },
          ),
        ),
      ),
    );
  }
}

class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  aMaps.AMapController _mapController;
  void onMapCreated(aMaps.AMapController controller) {
    _mapController = controller;
  }

  @override
  Widget build(BuildContext context) {
    // if (true) {
    //   return Scaffold(
    //     body: Container(
    //       width: 300,
    //       height: 300,
    //       child: aMaps.AMapWidget(
    //         apiKey: XMapsFlutter.aMapApiKey,
    //         onMapCreated: onMapCreated,
    //       ),
    //     ),
    //   );
    // }

    return Scaffold(
      body: Container(
        width: 300,
        height: 300,
        child: XMap(
          initialCameraPosition: CameraPosition(
            target: const LatLng(47.6, 8.8796),
            zoom: 16.0,
          ),
          markers: Set<Marker>.of(
            [
              Marker(
                markerId: MarkerId('marker_1'),
                position: LatLng(47.6, 8.8796),
                consumeTapEvents: true,
                infoWindow: InfoWindow(
                  title: 'PlatformMarker',
                  snippet: "Hi I'm a Platform Marker",
                ),
                onTap: () {
                  print("Marker tapped");
                },
              ),
            ],
          ),
          mapType: MapType.satellite,
          onTap: (location) => print('onTap: $location'),
          onCameraMove: (cameraUpdate) => print('onCameraMove: $cameraUpdate'),
          compassEnabled: true,
          onMapCreated: (controller) {
            Future.delayed(Duration(seconds: 2)).then(
                  (_) {
                controller.animateCamera(
                  CameraUpdate.newCameraPosition(
                    const CameraPosition(
                      bearing: 270.0,
                      target: LatLng(51.5160895, -0.1294527),
                      tilt: 30.0,
                      zoom: 18,
                    ),
                  ),
                );
                controller
                    .getVisibleRegion()
                    .then((bounds) => print("bounds: ${bounds.toString()}"));
              },
            );
          },
        ),
      ),
    );
  }
}
