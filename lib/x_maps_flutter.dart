library x_maps_flutter;

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart' as googleMaps;
import 'package:apple_maps_flutter/apple_maps_flutter.dart' as appleMaps;
import 'package:amap_flutter_map/amap_flutter_map.dart' as aMaps;
import 'package:amap_flutter_base/amap_flutter_base.dart' as aMapsBase;

part 'src/x_maps_flutter.dart';
part 'src/x_maps.dart';
part 'src/bitmap.dart';
part 'src/camera.dart';
part 'src/location.dart';
part 'src/marker.dart';
part 'src/polyline.dart';
part 'src/polygon.dart';
part 'src/cap.dart';
part 'src/circle.dart';
part 'src/joint_type.dart';
part 'src/pattern_item.dart';
part 'src/controller.dart';
part 'src/ui.dart';
