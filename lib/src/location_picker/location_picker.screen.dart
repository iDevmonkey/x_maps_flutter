import 'package:amap_map_fluttify/amap_map_fluttify.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'location_picker.widget.dart';

class LocationPickers extends StatefulWidget {
  static Future<Poi> show(BuildContext context) {
    return Navigator.push<Poi>(
      context,
      MaterialPageRoute<Poi>(
        builder: (BuildContext context) => LocationPickers(),
      ),
    );
  }

  @override
  _LocationPickersState createState() => _LocationPickersState();
}

class _LocationPickersState extends State<LocationPickers> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LocationPicker(
        requestPermission: () {
          return Permission.location.request().then((it) => it.isGranted);
        },
        poiItemBuilder: (poi, selected) {
          return ListTile(
            title: Text(poi.title),
            subtitle: Text(poi.address),
            trailing: selected ? Icon(Icons.check) : SizedBox.shrink(),
          );
        },
        onItemSelected: (data) {
          Navigator.pop(context, data?.poi);
        },
      ),
    );
  }
}
