part of x_maps_flutter;

class XMapsFlutter {
  static bool isChinaLocale = true;
  static aMapsBase.AMapApiKey aMapApiKey;

  ///
  /// 分为地图和接口服务(如Poi)，
  /// 地图：android国内使用高德，国外使用google；ios使用苹果地图
  /// 接口服务：国内使用高德，国外使用google
  /// {
  ///   "in_china": true
  ///   "config": {
  ///     "ios": "",
  ///     "android: ""
  ///   }
  /// }
  ///
  static Future<bool> init(Map configs) async {
    if (configs?.isEmpty ?? true) return false;

    if (!configs.containsKey("in_china")) return false;
    if (!configs.containsKey("config")) return false;

    isChinaLocale = convertToBool(configs["in_china"]) ?? isChinaLocale;

    if (isChinaLocale)  {
      Map config = configs["config"] as Map;
      aMapApiKey = aMapsBase.AMapApiKey(iosKey: config["ios"], androidKey: config["android"]);
    }
    else {

    }

    return true;
  }

  static bool convertToBool(dynamic data) {
    try {
      if (data == null) return null;
      if (data.runtimeType == bool) return data;
      if (data.runtimeType == int) return (data != 0) ? true : false;
      if (data.runtimeType == String) {
        int dataInt = int.tryParse(data);
        if (dataInt == null) return null;
        return (dataInt != 0) ? true : false;
      }
    }
    catch (e, s) {
      debugPrint(e.toString());
      debugPrint(s.toString());
    }

    return null;
  }
}